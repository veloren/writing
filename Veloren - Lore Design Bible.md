## Veloren - Lore Design Bible

[[_TOC_]]

SPOILER!
Be aware!

This text details information in regards to the Lore that otherwise is eventually supposed to be found out via exploration and finding artifacts and similar things to put together the mysteries and past of Veloren.

The Lore Bible is divided into two parts.
The Veloren Lore that goes over Velorens History, Cultures and other such Elements.
The Direction for Writers which outlines goals and directions for the games writing.

# Veloren Lore

## 1. Veloren - Historic Background

## 1.1 History of The Great Malady

More than a millennium ago, The Great Malady befell Veloren. The Dark Master planted his Velvet Black Banner** **on the shores of Veloren and brought with them a wildly varied and seemingly endless amount of soldiers, magic creatures and slaves. This massive host of deadly creatures and soldiers made up the nomadic troops and people of the Velvet Black Banner.

The Conquest was in full swing and the fate of elves and dwarves seemed sealed as somehow the Great Mind, the Master behind the Conquest, roared out in a cry of surprise and pain. The shout thundered over the land and all magic users under the Velvet Black Banner convulsed under a sharp pain as their master died. An abhorrent wave of wet, chunky and red explosions, pained shrieks and roares echoed over the front lines, encampments and cities under the invader’s control as most of their casters died.

Any magic user trained under control of The Velvet Black Banner below  a certain talent and magical strength simply died, their heads bursting, unable to withstand the pained deathcry of their master. Most of the Velvet Black Banners magical creatures just fell to the ground, dead on the spot they were standing on. Tens of thousand of cavalry troops, mounted on predominantly magical creatures, went to ground mid run, in a deafening and deadly avalanche of clanging armors and pain. Other creatures, many of immense age and might, only held under control by the Master's call, saw themselves finally freed, and left. Those that were of a more animalistic nature, corrupted or had simply gone insane at some point, fell upon their former allies and handlers to shred them to pieces, and either died doing so or fled.

Some of those creatures were generals of the army and joined together with other leaders, to voice claims on the leadership over the tribes and cultures under The Velvet Black Banner, even as their army struggled to recover from the death of most of their magic users. They needed a new leader fast, or The Velvet Black Banner would have grave problems.

Any resources that weren’t plundered from their enemies were gained through magical means, then refined and worked into usable shapes by forced labor and magic. Much of it needed to be conjured by magic because the crusade often left nothing in its wake but ash and plundered, dead lands.

The death of their master had not just killed most of the magic users, the magic that held the Velvet Black Banners slaves subservient had fallen away and slave riots broke out in the rear lines of the army. The anger from generations of abuse now unloaded itself onto slavers and foremen. Unable to unite under a new leader to take care of these problems, it was very clear that the host would soon run out of resources, most importantly food.

The troops of the Velvet Black Banner, a nomadic army so massive that its rear and resupply lines covered nearly half of Veloren, broke apart. After nearly a century of war against the Dwarven and Elven cultures, after driving them close to extinction, robbing them of their culture and leaving the Veloren continent a ruined dead husk of its former beauty, the Velvet Black Banner broke apart because they were going to run out of food.

The truth of the situation was that with their master’s bond over the troops gone many of the races and factions , corrupt, animalistic or otherwise, simply had only one thing in common: somewhere in the past they had been enemies. There was a week of silence. Rising tensions laid over the former allies as everyone tried to find out who was an ally and who was an enemy. Realizing that most all of them were enemies, absolute chaos broke loose.

The war had confined the Elves to the Pillar cities and forced the ailing Dwarves to a life under the Mountains. For both of these races the following decades of violent chaos and attrition was just a continuation of the invasion, as different factions tried to break the defense of their respective sanctuaries over and over.

Hunger soon engulfed and starved most of what was left of the once mighty Armies of The Velvet Black Banner, and the less  said about the horror of these decades the better.

Few of the troops or enslaved people managed to flee over the oceans, others degraded to small desperate tribes that managed to scratch together a  meager living off the dying continent. It was a living but it wasn’t much.

The results of this Era of strife and struggle are still visible in Veloren and have greatly influenced the cultures of today.

## 1.2 The Gold Elves - A Complex System

Elves are gifted with the ability to weave magic from birth. How great any individual's power will be depends much on the Elves' health and education. Just as a hungry human can’t run very far, neither  can an Elf suffering from great hunger cast great works of magic.

During **_The Great Malady_** that ravaged Veloren lands, Elves and Dwarves came together to build the **_Pillar Cities_**. Large Fortresses of seemingly impregnable stone towered over the landscape and helped the Elves defend themselves. Even after the invasion force of The Velvet Black Banner collapsed, the Gold Elves kept themselves closed off from the outside world for many centuries. A great number of the Gold Elves still live in the cores of these grandiose Pillars. Built with magic and the help of experienced dwarven architects, they are still the most important centres of Gold Elven civilisation and power. 

### 1.2.1 Gold Elves - Nobles, Merchants and Workers

live predominantly in these Pillar Cities. The City Kingdoms of the Gold Elves outwards wield much political and economic power and play a great role in the events of Veloren. What most people know of the Gold Elves is their life in the glorious and shining cities on top of the pillars. Though most of them live in the dense and countless halls of the city cores to which only few outsiders have official access. To the outside this makes them appear as a people of great unity, order and strength. A People  that would never yield to anyone. 

But the truth is far from ideal as the Kingdoms are afoul with corruption and greed. The powerful on the top holding their might over their people in the Pillars, making it hard for anyone that doesn’t have enough influence or coin to leave the bowels of the cities. The lower one treads in the city the desperate the circumstances become. The people are strained and political turmoil is a constant. Great speeches to unity, elven power and patriotism ring evermore hollow as the workers toll and live in terrible circumstances. And the narrow tunnels and shady Chambers hall from whispers of revolution.

Most Gold Elves that the outside world gets to see are from influential houses or rich families. With fair, healthy complexion and a slight tan as this is the privilege of the Surface classes. But more and more dwellers of the deeper layers find their way outside. Most of them leave the cities in service of the Elven army that recruits their foot soldiers from the less fortunate populace of the deeper layers of the city. But more of the average citizenry can be seen these days as they are needed to spread Gold Elven power not only in political influence but also to grow Elven reign over the land. Wishing to be less dependent on the food deliveries of other Powers the Kingdoms need to uplift farmers and workers to the surface.

These Gold elves show a much paler skin tone due to generations of living in the tunnels and halls of the Pillar Cities. Most wear wide cramped hats, long sleeved clothing and tinted glasses in woven, wooden frames to protect them from the full shine of the sun. Most don’t dare use that chance to flee the Kingdoms grasp, as they fear being hunted by the order of the Dark Elves or retribution done to their families at home.

### 1.2.2 Dark Elves - Controlling and Policing Cultish Order

The ancestors of the Dark Elves were once those Elven battalions that held the gates of the Pillar Cities, when Velorens surface was ashen, barren and infested with enemies that regularly tried to invade the Gold Elves sanctuaries. 

Resources were always tight in the Pillar Cities and they were often plagued with terrible famines, hunger riots and decades without Law and Order. The Population of the Gold Elves were constantly, terribly shrinking and with that more and more of the cities lower levels were abandoned.

Often alone these Gatekeepers, sworn to protect its inhabitants from the outside world, were able to keep any kind of order. Tired to see their own people rip each other apart and losing the pride of their culture they turned inwards. Through hard fighting they brought back control and order to most of the cities.

To prevent future chaos tight laws were created, often strict rules that were necessary so that the Gold Elves could survive through the centuries, despite the lack of resources and the dire circumstances. Over time these gatekeepers became the Order of the Dark Elves, keepers of the law and stability. 

Over the centuries the order has become a people within the people. As the few of the Gold Elves that for thousands of years experienced sunlight the Dark Elves developed a much darker skin tone than other elfs. The tradition to marry mostly within their own ranks exacerbated this difference between them and the rest of the Gold Elves.

The Dark Elves have become near zealous in their vow to protect the Pillar Cities from its enemies and keep them from falling to chaos. Lack of Loyalty towards Gold Elven culture and its center around the Pillar Cities is taken ill with grave seriousness. Where Dark Elves walk they awake fears in the Gold Elven populace. Not even the kings are safe should the Leaders of the Order deem them unfit and think they might damage the security of the Fortress Cities. 

As the Pillar Cities are opening up and much food and other ressources stream into the city, the Gold Elven populace is growing healthily again, stretching their forces thin. Unsure of their orders' purpose in this changed world, some of the Dark Elves become only more extreme. On the opposite, corruption is spreading within some families of the Order.

Dark Elves are seen as relentless in their beliefs and as near zealous fighters for the order and honor of the Pillar Cities. They are easy to identify with their nearly bluish-black skin tone and their uniform like clothing.

The Dark Elves harbor a deep seated hatred against the Brushwood Elves, for their choice to side with the humans during the War of Awakening.

### 1.2.3 Pale Elves - Secret Council and Feared Advisors

are the mysterious wise of the Golden Elves. An order of seemingly creeping, and extremely secretive Elves, they live deep, deep below the most desolate layers of the cities, in dark and foreign chambers. There they dwell and research ancient knowledge or wander through far off dark halls and caves. For what purpose no one knows or dares to ask. Only few make the perilous journey downwards to the Pale Elves halls to seek their advice, knowledge and blessing, though regular food and supply deliveries are being delivered downwards. No King of the Cities can be crowned without visiting them and seeking their approval. The Pale Elves also hold great sway over the Order of the Dark Elves. From time to time it happens that Elves can be seen walking downwards like in trance. This is seen as a sign that they will join the order. No one would dare to stop these poor blessed souls and doing so can incur the wrath of the Dark Elves.

Barely anyone sees them nowadays. Especially not Outsiders since as the Pale Elves do not walk under the sun. Being mysterious pale creatures in the dark one would be greatly amiss to underestimate their power and pull over the surface. Many have fruitlessly tried to end their influence and paid dearly for it. 

## 1.3 The Brushwood Elves - The Kingdome   

For thousands of years the ancestors of the Brushwood Elves fed the Pillar Cities. Working constantly and no matter the season they tried to keep up with the hunger of the populace. Never demanding, always churning the dirt and trying to become better and better at prying fruit from the ground. When the city erupted in chaos they kept working. When the Dark Elves invoked evermore strict laws to restore order they kept working. For centuries just strived to feed as many as possible. 

To refresh the dirt they worked with, their farmer ancestors ventured out on dangerous expeditions under Dark Elf protection, to seek dirt that was not as dead as most of Veloren’s ground after the Velvet Black had been through with it.

After more than a thousand years the ground began to recover more and more. 

When the Dark Elves kept the cities closed as the land was barren and damp, the Brushwood Elves fled the cramped desolate cities. With them they brought seeds from the vaults of the cities. They also brought a secret with them that few knew of and that had been given from generation to generation since the Pillar Cities had closed their gates to the outside world. Escaped from the confines of the cities they started planting and growing plant life. It was these elves that started Velorens long recovery back to life. It was these elves that heeded the first human nomads welcome in the then barren lands and helped them settle in. Most human cultures have since forgotten much of this but together they worked and toiled hard and raised a new green Veloren. It was also humans and Brushwood elves together that stood against the Orcs when these first appeared on Veloren. Seeking to undo the hard work of humans and elf they tried to plunder the young forests and savage the fresh grown meadows. The Brushwood elves never forgot this and many of them harbor a warm friendly memory and attitude to their old friends, even when those people couldn’t keep the memory. Even when the humans threaten to fight wars against the elves most of the elves effort is to prioritize peace with their Kingdoms.

In the center of the Brushwood’s ever reaching forests grows the oldest tree on Veloren. Visible from near every corner of Brushwood land, its presence strengthens the Wood elves’ magic. Though many have seen it due to its size, only few foreigners have actually had the honor of visiting it, most of whom were humans since the elves still honor the diligence of the human ancestors that helped heal Valoren wounds with the hard work of their hands.

Because of this the Brushwood Elves did not hesitate a moment when the Gold Elves attacked the young human settlements immediately after leaving the Pillar Cities for the first time. Brushwood Elves can be rather hostile to the Nobles of the Gold Elves. Banditry aimed at Gold Elven nobles venturing too close to the Brushwood isn’t seldom. No Gold Elven Army has left the Brushwood Forest after they entered it and the relations between them and the Brushwood Elves are still hostile today.

There is a story from a few hundred years ago that tells the saga of a brave Orc Chief that slipped through the Brushwood elves guards to steal a seed from this holy tree to give to her tribes. Which is not quite the truth.

## 1.4 The Dwarves - Dwarven Union

The Dwarves once had a great and varied culture spanning the colder zones of the world.

Great Dwarven Cities wound along most Mountain regions of Veloren. Dwarves and Elves were close Allies and friends. Exchange of culture and trade between them was stable and complex. The two cultures were woven tightly with one another. So when the darkness rose on the Elven borders the Dwarves did not hesitate and took up warhammer and axe to aid their friends.

For centuries, Dwarves and Elves fought together in The Great Malady against the armies of The Velvet Black Flag. Together they devised and built the Pillar Cities, mighty Fortresses as the last refuge of both their cultures. Allied army after Allied army, in the South as in the North, stood desperately before the wave of enemy soldiers and corrupted creatures that were burning and ravaging the fruits of the dwarven and elven civilisation, in valleys and plains as well as in the Mountains. Together, architects and magicians erected the Pillars with great effort and under disregard for the personal lives of Thaumaturge and Workers. And as the Pillar Cities neared completion the Elves betrayed the Dwarves.

The great gates of the cities closed off for any and all Dwarven people and they were swept away before the Darkness. Without choice they drew back into their great mines and their workshops in the mountains where they tried to hold out as long as they could. As the Dwarven Culture seemed lost、an unlikely ally came to their rescue. An ally that asked the dwarves to forgive them for their part in the destruction of Veloren.

Like the Elves the Dwarves pulled back into their sanctuary in the mines and closed their gates for many centuries but opposed to the Elven people the dwarves have not forgotten those that helped them and those that betrayed them.

While the dwarves stayed inside their mountains to recover their civilization, they still sent out foraging parties to look for wood, seeds and fresh animal stock. And they still sent scouts to keep an eye on the outside world. When these scouts reported that the Gold Elves attacked early human civilisation, the Dwarves opened the massive gates to their subterranean cities. And there was no mistaking their ambitions as they aided the human kingdoms in the war against the Gold Elves. Nowadays most settlements outside their Mountain Cities are massive Workshops, an ocean of smoking chimneys under a sky of black smoke. Day and night, countless hammers work metals and stone into tools, weapons and hammers. Out of the  range of smoke and soot, dwarves till massive fields of crops, vegetables and mushrooms to fill their storage with food and improve the quality and nourishment of their diets. Infinite tunnel systems draw through the mountains to track down even the last chunk of metal in the ground. The Dwarven army is ever growing, the draft being seen as a great honor for every citizen. Dwarves leaving the Army only do so when they know they can be more help in the workshops and industries. But every single Dwarf, even those not part of the army, regularly come to the massive Barracks that are spreading over Dwarven Land to practise drawing down a mighty hammer and pulling a sharp axe. Military discipline is in every facet of dwarven culture. Even workshops using ranks to discern between Adept, Foreman and Master worker. Outwards the army is protecting dwarven lands and fending off mysteries stirred awake from the noise of the dwarves ever growing mines. But this Culture of craftsmen and grim fighters are focused on another grim goal. A goal given to every dwarf, old enough to listen to tales of the grim past and adult enough to keep the silence about the path to the future. Much of Dwarven Culture seeks to gain just payment for their ancestors' work, to remind the Elves of their cowardly betrayal and to take back their share of the Pillar Cities they once helped build.

## 1.5 The Humans - Kingdoms

Long after the fires of The Great Malady had stopped burning, many of its beastly creatures, and splintered factions roamed the world, some wandering far beyond Velorens shores. And in flight from one horde of such creatures Human settlers first left their Islands. After a perilous journey they finally landed their boats on the shores of Veloren.
At this time Veloren was a barren Land with only pockets of green growing in hidden crevazes and valleys. They had brought seeds with them but most humans so far had lived from fishing, hunting and gathering of fruits and wild vegetables. Here and there they had smaller fields but not enough agriculture to feed themselves with it. Unable to turn back the humans spread over the land hunting the few edible beasts they could find. The humans became Nomads and travelled a Veloren that was for the most part still void of any Elves or Dwarves since these at that time still were holed up inside their Fortresses.

However after a few generations on the lonely continent the Nomadic Humans met the Brushwood Elves (which they didn’t call themselves yet since there were no woods yet).
The two groups immediately became close allies and later friends.
The Elves taught the humans animal husbandry and farming. And they told them of The Great Malady that wounded the land and their vision to heal Veloren.
Over the years most of the nomadic humans were drawn to what today is the center of Brushwood. Under the diligent hands of Human and elves the green spread and grew.
Animals, before hidden and spread sparsely far over the empty surface of Valoren, flourished again and carried seeds over the land, spreading the green growth even further.

Together they also fought off the first contact and clashes with the Orcs tribes, who now and then befell the greening land and were ravaging the borders of the vulnerable vegetation, avariciously seeking the wood of the still young forests. But this remains just a side note in history, since the roaming Orc tribes at that time were more busy fighting each other then elves and humans.

While the Elves stayed with their growing Brushwood Forest, soon taking on the name they gave it for themselves, the humans spread out thinly over the continent. With humans establishing farms and forresteries plantlife grew back fast, all over Veloren. And thanks to the nutritious ash and seeds spread by animals it got a stable hold.  For a long time humanity could grow undisturbed and comfortably. Many regions were and are still thinly settled. From the Brushwood forest to the coast where humanity had first stepped on Veloren, a vulnerable band of civilisation, small Kingdoms and tribes, had been established. There was trade and little exchange of culture. But the connections were loose at best and everyone kind of did their own thing. And for a time Veloren was at peace.

Then the Gold Elves opened up their cities and sent their armies into the world outside of their Spire Cities. Assuming everything outside to be their enemies, they attacked human settlements and drove them from their homes and fields. Pushing human civilization along its thin line of settlements caused the many Kingdoms and Tribes to unite. Together with the Brushwood Elves and experienced from the conflicts with the Orcs they stopped the surge of the Gold Elves. The war threatened to become a longborn and dirty stalemate as armies of Dwarves marched down from the mountains. Roused from their Underground Cities by the sound of fighting the dwarves did not hesitate to fight against the Gold Elves with great fervor.

After the war ended with a truce the rulers of the once sleepy domains of humanity recognised the signs of the time and banded together. Then some of the split up and fought each.... Oh no wait they have peace agai… oh... what? Now the south fights the north and… ah peace again! No wait… ah whatever. Safe to say humanity started spreading their political and cultural influence, fought many wars small and great and learned enough to be a challenge to the reemerging Gold Elves, the Dwarves or the Orcs. This started the **Age Of Competition**.

Not having suffered such a historic genetic blood loss to their people (and then being trapped for centuries in relatively close confines if fortresses) as it has happened to Elves and Dwarves humans are extremely diverse in stature, tone of skin and all the other markers that make a person unique. And humans also have no qualms to seek their live partners within the other races of Veloren what makes the whole populace just even more colorful.

## 1.6 The Orcs - Tribal Union

Not even the oldest Shamans know where the Orcs once came from. But they know how the Orcs arrived on Veloren. They arrived on Velorens shores as one of the many subjugated races under the Velvet Black Banner.

The Armies of the Velvet Black banner only considered magic users of a certain talent as worthwhile and worth training. Either they were able to great and powerful magic or they weren’t trained at all. The wildly different races and creatures of the invaders were held together as allies by a magic that dimmed most magic users mind. So many of the more powerful casters had problems to stay rational and off a clear mind of their own. Instead fanaticism to their dark leader and to their cause of conquest was a prevalent mindset. But fanatics with a blinded mind make bad soldiers in pressing and critical situations. 

This did not affect most of the Orcs. Orcs had either a strong connection to magic or none at all. Though Orcish shamans were under its influence, the great majority of the Orcs was not affected by the control magic. It was this ability of the Orcs, to keep a clear mind of their own, which saved their existence as a species. One of the Generals offered the Orcs their lives for their service as he saw great problems in the blind fanatic rage and the mindlessness that was inherent to many of the Velvet Black Banners troops. In the conquest of Veloren this mindless rage cost the invaders many victories as Elves and Dwarves knew how to fight and defend their cities with great skill. What was usually a matter of years became a matter of decades. And so the Orcs were dragged from one burning frontline to the next, dying while trying to make up for the incompetence of other generals.

The tribes of the Orcs were part of a siege on a Dwarven mine when the Master of the Velvet Black Banner died. The deathcry of the Master killed most of the orc shamans instantly or drove them to madness. As the unity of the Velvet Black Banner fell apart and into chaos and open conflict the Orcs rallied all of their tribes.

Not knowing where to go in the chaos, they went to the only faction that showed some form of order and sanity. They went to the dwarves. The siege was half in progress and half falling apart as the Orcs swept through their disorganised former allies coming to the dwarves aid. Desperate and at the end of their rope from decades of war, the dwarves simply had no other choice as letting the green skinned warriors join them in the defense of the dwarven halls off their mountains.

First uneasy partners in the fight for survival, fending off the frequent attacks of disheveled Velvet Black Banner troops, they later became genuine allies. In the end the dwarves would have gladly let them stay within their halls. But the Orcs felt great guilt when looking out over the barren, destroyed Landscape and the ashen city ruins of Veloren. The Orcs swore to clear the land of whatever remained of the cursed Velvet Black forces and the creatures they had brought to the continent. This hunt is what made them the hardy warrior culture they are today, charging towards any enemy that dared to cross their tribes. Only few Orcs are left today that know their history tough.

At the start of the Age of Competition most Orc tribes were fractured and without great unity.
Elves and Dwarves took more and more to the surface and the Humans reformed into greater Kingdoms and spread their territory and influence. A group of Orc Chiefs was greatly worried for the future of their kin. They still had barely any casters which put them at a great disadvantage and what culture there was, was fleeting and slowly degrading. In their plight the proud Orcs overcame their pride and went to the Brushwood Elves. Personally hearing their inquiry for help, the Queen took the group under her own tutelage to teach them knowledge that would be needed to nurture culture and find a solution to the Orcs lack in magic. After many years of studying and experimenting they found a way. For this the Elves Queen presented them with seeds from Brushwoods first and most sacred tree for the Orcs to grow into magic focus.

Thankful, the Orcs swore to stand by them should Brushwood ever be in true and grave danger. They asked for just one last favour. As the Orcs were now the tribes would never accept help from outside their kin. So, to make this help work for them, the Chiefs would spin a tale in which they stole the seeds in a daring action from the Elves holiest tree. The Queen agreed and the Orc Leaders left the Forest to unite their people under one Chief, driven by this new hope. The Rest of the Chiefs took up the role of Shamans and they grew the seeds into holy trees. From the Sap, the seeds and fallen wood of these trees they crafted talismans and strong foci and trained a new generation of orc casters. Restored to such strength, the Orcs gained new resolve and an equal place in the contest of nations.

However, not long ago the last great Chief died during the Hunt after one of Velorens hidden evils. And foolishly she left without appointing one of her children as successor. This mistake was to split the Tribes.

When The Undead Protectorate came to life it split the tribes in two. Sworn to eradicate the darkness of The Great Malady from Veloren, one of the Chiefs children insisted that the Undead must all be destroyed, as holy duty to the Hunt. The other Child though countered, pointing out the peaceful ways the Undead had chosen to live by and the fact that they had been made against their own will. In closed, knowing company the First reminded his sibling that they were sworn to destroy all corrupted Remains of the Velvet Black. The Second reminded him that their kin only were here because they helped bring the same kind of corruption to Veloren. With the question of succession to the position of Chief unclear the tribes were split in two. Only the Shamans' refusal to take sides prevented an outbreak of violence and Sibling war. So today the Orc nation is split in two, veering close to a sibling war and their success as a nation is in peril.

## 1.7 The Undead Protectorate - King and Council

The Age of Competition brought development and growth. Worldly Craftsmanship and the Arcane Sciences made great leaps. Culture bloomed. Expanding Trade carried the knowledge, culture and ideas far and wide over the land.

In search for more profit a group of Aetherial Arcanists researched new ways to create a better and cheaper workforce. They first tried for the creation of golems. But the cost in magical power and physical materials was greater than what the use of Aetherial Golems as a workforce would bring in profit. Naturist Casters refused to aid the research by using Spirits of nature or even using life force to fuel such a force.

So, in a time full of turmoil and small but numerous conflicts they went for something in between. They went for a resource that was plenty in these times. They created a workforce from the freshly deceased. It took some time, but when they presented a full workforce of undead working a field day and night in presentation to a Human Duke. The Lord was immediately fire and flame and agreed to financing the further development of this new kind of magic.

Even when the Reawakened were only able to perform simple and menial tasks their application quickly caught on with Landlords and the owners of Manufactures, Traders and organised Haulers. Early discomfort and disgust in regards to seeing the dead walk again vanished rapidly as it became clear what Profits could be made from this cheap, resilient and obedient Labour force.

The Reawakened were mostly used in Human Territories. The Dwarves respected Labour too much as a sacrosanct honor to let once dead people do actual labor and besides a few uses in hauling inventory they were never much used. The Gold Elves already had extremely cheap and (in the nobles' opinion) obedient labour force so there was no need for the Undead. The Brushwood Elves just shook their heads and basically kept an unending stream of polite diplomatic messages coming that the humans may reconsider this thing.

It took a few years until normal people of Humanity realized the consequences. The backlash from the working class when they did was unprecedented. Especially Dock workers, Farmhands and similar tasking vocations had no qualms to voice protest. The back and forth was fiery. Protests followed after protests, more than once all out riots could only be prevented by sheer luck and thanks to some calm heads on both sides. Then Human territories all but burst into flames when it came out that Owners were plundering the peoples graveyards. A revolution of the working class seemed inevitable when, suddenly, massive Orc armies stood on humanity's borders!

It took a while until wandering Orcs reported to their Shamans about the spread of the Undead Workforce in human territory.  A group of Shamans then went to take a look at the matter themselves. They needed exactly one look upon one of the Reawakened humans to immediately recognise this as one of the many old evils their ancestors had described. Seeing that, the Great Chief wasted no time and the tribes came together and marched to the borders of human territories.

And then, on the height of the Undead crisis, the Beloved Undead King was created. A Queen of one of Velorens most powerful Kingdoms wanted her beloved husband back. And as he stood up from the table that they revived him on, he lamented the suffering of the undead, declaring he could hear their suffering voices as they were enslaved and tormented, branding the practise as barbaric in loud protest.

So the workers' protests escalated and in many cases closed ranks with Orc Leaders as these were openly declaring they would rid the Veloren from the Reawakened, if necessary by force. The name Undead caught on quick. The Dwarves and Brushwood Elves became increasingly queasy towards the whole matter as the technique to reawaken the Dead improved daily. And one of humanity's most beloved and now Undead King and his allies damned the practise and treatment of the Undead as barbaric, ready to strike out in defence of the Returned Souls.

A Truce was made. The Kingdoms with rioting workers and Orcs at their doorstep were just relieved to be offered a way to rid themselves of the cause for this crisis. The Undead King would take up the refugees, now named Undead. The Treaties against Slavery were expanded upon to directly recognize the Undead as individuals. Nonetheless, it stayed a crisis as the masses of Undead moved towards the Lands that united under the Name of The Undead Protectorate. Orc or otherwise motivated attacks on Convoys of the Undead were not seldom and the Protectorates Soldiers and Nobles became more aggressive in defending them as their devotion to their King took on a Cultish shade.

Most everyone was relieved when, finally, the last Undead arrived in the Protectorate. Over the generations, the contact between the lands of the Protectorate and the rest of the world got rarer. However, great advances in the medical fields became one of the protectorates greatest exports, as they learned much about the human body in the necessary work of maintaining the Undeads Bodies. They also export great amounts of ores and stone since they are not tiring when mining them and it makes sense to use this advantage to bolster their trade.

The Culture of the Undead Protectorate is a calm, sombre one, due to the fact that most of the Undead people had been forced into existence. It is a culture of great care and healing for the body and mind. Their Cities are places of friendly and respectful whispers even on the busiest days. Their numbers keep growing as no few people seek to escape death or to restore a loved one, taken too early, back to unlife. In their lands the countryside is virtually untouched since the majority of the populace by now is of the Undead and they found new ways to feed themselves rather than the wet process of digestion. They love all living things and try to protect the living just as much as themselves. Any new born child of the living populace is seen as a great reason to celebrate. Philosophy and discussions about life and death and the sense of being are important within the Protectorate. Much time is being devoted to these and other arts, since there is much time to give to these. In other words; they are great friends with the Brushwood Elves.

They prefer the nights and a dry cold, they really don’t like rain, the jungle or deserts though (since humidity and too dry conditions both are not good conditions for their bodies). Sexual attraction is not really a thing with the Undead.

The Undead Protectorate would be a relatively peaceful place if not for some of the Orc tribes that were occasionally invading their lands and some of their own, undead citizens falling to insanity or harboring less peaceful ambitions. So The Undead Protectorate was forced to build an Army of their own. The Restless Marching are incredibly disciplined troops, never distracted by drive or swayed to cruelty by fear or anger born from fear. They have no end of stamina but that their bodies can suffer from Material Fatigue. And those devoted to protecting their land do not stop practising, ever.

All in all The Undead Protectorate prefers peace and would like to grow their diplomacy and trade, sharing their culture and ethereal and necromantic science. But if necessary they do not shy away to send their strongest weapon, Undead Assassins. Breathless and free from bodily bother and need (does not need to eat, go for little ghuls or fear cramps from sitting still in one spot for days or weeks), there are few things that can keep an Undead Assassin or Spy from reaching their goal.

## 1.8 The Danari - The Republic

By the time the nomadic Velvet Black Banner came to Veloren, the Danari had long been the bureaucratic backbone of its administration and organisation. By then, their entire culture had served the Velvet Black for so long that their home country was a distant memory.
The vision that bound their people to the Velvet Black Master was that one strong leader could unite all peoples and lead to a final and everlasting peace. 

The long, drawn out war against dwarves and elves had strained the Velvet Black’s armies and damaged that vision. Veloren’s people held steadfast and solidly over decades even as they lost battles every day. Unaccustomed to such a difficult war, their Leader became irate and unbearable, even for them. And when their master’s impatience and discontent grew into cruelty the Danari were front and center and its first recipients.

It took a while, since the vision for universal peace through conquest was so integral to the Danari’s view of their place in the world. But after many secret meetings and Council sessions behind closed curtains the Danari decided to act.

Finally, they used the bureaucratic might they held over the Velvet Black, isolated the highest Leaders of the Black Velvet Court and their Master, and struck.

The Danari not being warriors at that time, their bravest stepped and struck one of the Leaders after another. Few of these even had an inkling as Danari struck them down, but the few that did put up a desperate and angry fight. Although these fights were often short they were just as violent and many daring Danari died.

Most of them died fighting the Master himself in the Dwarven Tower their master had chosen as a seat to oversee the conquest of Veloren from. Knowing of his powers, a good hundred Danari had taken to its servant corridors and had exchanged places with bureaucratic personnel. The battle was brutal and deadly. But despite the Arcane Might their Master threw against them they drove him up to the highest chambers. Dead Danari were strewn about the great Chamber at the tower’s top as one of the Danari finally managed to tackle him and grapple him over the edge. Falling over the railing the falling Arcanist attempted to use his magics to save himself. But the Danari he had dragged with him didn’t let loose. And while they fell the small person kept ramming his knife into the former Master’s chest.

Beneath his bloody breath he started a curse, but died before he even hit the ground. So only half an unfinished curse befell the Danari when he and the Danari slammed into the sides of the Dwarven tower and then the mountain the tower was built upon. No one knows what the curse was meant to be but as the day went on and the consequences of their Master’s death rippled through the Velvet Black Army, breaking it apart, all Danari felt it harder and harder to breathe and felt drawn to bodies of water. Most likely having intended to drown them in some cruel way their Master’s dying mind had given them the ability to breathe underwater, which banished them under the ocean's surface. And that turned out to be their greatest blessing.

As the Velvet Black Army fell apart the Danari organised their people’s flight to the oceans, keeping most of their recordings and culture intact as they had to move under the ocean. As the people on Veloren’s now lifeless surface either had to ration their food or were starving to death, the Danari lived from the riches of the surrounding oceans and built a solid culture. Their ability to be beyond the reach of Veloren’s evil protected them from cultural degradation and the same level of crisis that the people on Veloren’s surface had been subjected to. So on the edges of the oceans their culture continued to develop and grow. Nowadays the Danari have given up on the idea that it needs one strong leader, and developed instead a more democratic system.

By the Age of Competition the incomplete curse had greatly weakened; the longer a Danari spends on the surface the more they lose the curse and with that the ability to breathe underwater. It was also during the Age of Competition that they first emerged and showed themselves as a power to be reckoned with.

With the growth of trade between the Cultures and Kingdoms the search for new tradelanes exploded and soon turned to the waters. Until now it was mostly known as the domain of fishers, faremen or loggers. Exploding investment by Merchants was soon followed and matched by their rulers that didn’t like the possibilities the sea offered to evade trade tariffs and taxes. And as soon as one King drove his army directly up to the Capital of his adversary, the seas were filling with increasingly better ships. 

Wars were about to break out over who ruled the seas. This is when the Danari came out of the water in force. Until now they were seen as just one of the many weirder tribes that exist here and there on the edges of civilisation. The delegation of several thousand Danari in formation surprised the land-dwelling rulers. But in the end they were sent home without anyone taking their warnings seriously.

So the Danari were forced to make their point. Over the next year Danari damaged merchant ships from below the water level, forcing many of the crews to leave the ships and wares behind or run them aground on nearby safe coasts. The next time the Danari sent a delegation to various rulers they were welcomed with full seriousness and honors.
Today any kingdom that wants to extensively use nearby waters for trade has contracts with the Republic in place. This power over the sea is also what affords the Danari a lot of leverage in political matters. 

Most Danari live under the sea level in the oceans and great lakes of Veloren. But they do have many settlements on the coasts or in deep valleys and plains when those are not far above sea level or far from great waters. On land their bedrooms have cozy sleeping pits which are technically under sea level so the curse doesn’t wear off as quickly. The average Danari regularly visits the nearest great body of water to renew the blessing that had protected their culture for so long.

Though keeping parts of their long past secret, especially towards the Orcs that see in them victims that need support, the Danari still believe in a uniting peace. But they have changed the way they want to promote this ideal. Instead of conquest they try and peacefully spread the ideal and their democracy and culture. They have even started to open up their porcelain cities by building towers that connect to the surface and allow in visitors.

Their views are not being well received by many of the rulers. So while they are often tolerated, their books and philosophers often find themselves on banned lists, and the Danari pressing matters can and has led to conflict. Though they have found good friends and great philosophical debate in the people of the Undead Protectorate.

## 2. Historical Timeline

The dates listed here start with rough estimates, since not many accurate accounts of ancient times are known that could date these more accurately. \
The further we get into nowadays, the more detailed the dates get thanks to a growing effort of the civilised cultures to document them.

### 2.1 Age of Wonder 

Elves and Dwarves live together in an age of peace and prosperity. Not much is known about this age because of what ended it and destroyed or twisted most evidence, cultural or otherwise, from this time. Most don’t know it ever existed, nobody knows how it came to be, how long it lasted or what it actually looked like.

### 2.2 The Great Malady [Year 0]

The armies of the Velvet Black Banner invade Veloren and draw a line of destruction over the continent. The Orcs are one small part of this massive tross.

The war would drag on for decades.

### 2.3 The Betrayal [est. 30 to 40 AGM - After Great Malady]

After decades of war the Elves betray the Dwarves, barring them from sanctuary in the Pillar Cities that the dwarves helped create.

### 2.4 Velvet Black Army Falls [est. 50 to 70 AGM]

The Armies Master is killed. The spell forcing the many races and cultures together is gone. The army falls apart.

The Danari are cursed to a life under the sea level.

### 2.5 The Years of Chaos [est. 70 AGM and centuries after] 

The Armies of the Velvet Black rip themselves apart because barely a  \
thing grows in Veloren any more without magical support, the slaves are dead or fleeing and part of the army has lost their minds attacking anything within reach. 

The Orcs aid the Dwarves in exchange for sanctuary. They become allies. 

### 2.6 The Long Dark. [est. from 70 to 150 AGM] 

Elves and Dwarves isolate themselves in their Cities/Mines. \
The Orc tribes go on a crusade sworn to rid desolate Veloren of what’s left of the Velvet Black Forces. Over hundreds of years groups left from The Velvet Black fight each other as well as attack the sanctuaries of Dwarves and Elves till the last remnants vanish, hide in distant corners of Veloren or simply die of starvation, illness or injury. 

### 2.7 Revolt Of The Farmers [est. 1200 - 1500 AGM]

A group of Elves flees the confines of the still closed of Pillar Cities in search of a better life. From the Cities Vaults they carry thousands of seeds that they start to plant in the ashen lands.

### 2.8 The Farmers Age. [est. 1500 to 2100 AGM]

Nomadic humans arrive in Veloren. Traveling the desolate Lands they meet and befriend the Elves. Together they start healing Veloren with hard work, regrowing the greens on its surface. The Elves start calling themselves the Brushwood Elves, and humans settle all over Veloren in small numbers. Aside from the one or other attack from primitive Orc Tribes it is an age of peace. 

### 2.9 A Plea For Help - Oldest Recorded Event [est. 1830 AGM]  [370 BAC - Before the Age of Competition] 

Desperate as to the desolate state of orc culture an Orc Chief asks the Brushwood Elves for aid. He receives education from the Queen and a seed from the Elder Tree. With this he starts uniting the Orc tribes.

At the day of their leave the Chief swore to repay the debt. To keep track of this the de facto Ruler of the Brushwood Elves starts tracking the days since this promise establishing it as a new day zero of concrete records and dates of history. 

However with this event being a secret between Orcs and Elves the first officially recorded date was placed 26 days later, at the first day of the Harvest Festival. A seasonal Celebration of a successful harvest that Brushwood Elves and Humans celebrated together.

### 2.10 The War of Awakening [4 BAC]

The Gold Elves finally leave the isolation of their Pillar Cities immediately attacking human settlements. The Brushwood Elves come to the aid of the Humans. It also causes the Dwarves to open their mines and assist the Humanity against the Gold Elves. The War ends after four years in a truce and begins a new age on Veloren.

### 2.11 The Age of Competition [0 AC]

Historians agree that the year the war of Awakening ends in a truce started a new kind of Age. The truce is unique in that it was a document signed by all parties of the war. The only people excluded were the Orcs that didn’t participate in it, and the Danari who hadn’t made contact with anyone else yet.

It took a few decades, but since most leaders or heads of states of the time had this one event in common it soon became the year one of the modern historical date. The name Age Of Competition was added several hundred years later by Court Historians using the name extensively in discussions with one another and then on the courts of their donors and financiers.

In the coming centuries after the war, new Kingdoms and cultures form that compete with one another politically, culturally, economically and within the sciences. At the start of the age the lines are drawn rather clearly but over time new alliances and Kingdoms rise, fall, rise again and change. Wars are fought, though none reach the dimensions of the great Malady, trade and culture flow and ebb between the races and countries. And in the lost ruins of the past, Artifacts, Treasures and cursed Creatures await adventurers, researchers and other curious fools.

### 2.12 Rulers of the Waters [185 AC]

The Danari emerge from the oceans and great seas to seek diplomatic exchange to prevent wars and abuse of the waters they live in. They are being more or less laughed out of the courts until they wage a campaign against merchant and military ships establishing themselves as a serious power on Veloren with contractual binds and connections with any nation with extensive waters nearby.

### 2.13 The Undead Industrial Revolution [464 to 466 AC]

A Group of Magicians researches the idea to use the physical bodies of the deceased as a cheap labour force. Great protests of the living workforce erupt. The practise is being banned as better necromantic techniques lead to re-awakened Undead that are more or less in possession of their mind and memories. A newly Undead King seeing their plight calls for all the shunted Undead to seek refuge in his lands. The Undead hear and follow the call. The Undead Protectorate comes into existence.

### 2.14 The Split of the Orc Tribes [466 AC]

The presence of the Undead Protectorate splits the Orcish people in two as they disagree fundamentally on the nature of the Undead People. One half determined to eradicate them and the other respecting them as a peaceful albeit tragic people.

### 2.15 The Seekers Guild - 475 AC 

Things seem relatively peaceful. Wars had mostly subsided for the last 4 years.
As civilisation seems to take a breather, brave explorers hire bored mercenaries and make a find far outside the civilised world. They find and open what seemed to be the underground portion of a long ago destroyed fortress. And from there they find and bring back great treasures. Stories about the artifacts spread like a wildfire. And they start a whole feverish rush for artifacts and treasures.

Unfortunately people tend to only remember things they like. And so thousands if not tens of thousands of seekers of wealth and power rush out, unprepared, panicking they would miss the opportunity.

Over the next two years what is left of the rush dribs back to civilisation. Most of the returning are half starved, sick, sometimes cursed and near always traumatised. And they carry the curses and worse back to civilisation.

After cases of spreading curses, illnesses and cruel deaths of all kind by crazed, cursed or corrupted returners, wily merchants, thaumaturges, researchers and wary rulers found the Seekers Guild. An organisation to train and manage willing adventurers, keep an eye on the circulation of artifacts, research new and old knowledge, support the brave that venture out and facilitate safe trade of whatever is being found in the ruins and corners of the Wilderness.

### 2.16 Today! - 498 AC

It is not even two years till the year 500 AC. It is THE date that many look towards. Many, many people have been already planning for this for years. Festivities are being prepared all over Veloren with few exceptions. Also in preparation is the Adventurers Guild who will celebrate its 25th year of existence and its continued and successful spread as a sort of neutral entity, a central point for not only adventurers but also the various services and craftsmen like armorers and smiths, that are needed by them. People that seek to hire a group of well trained and experienced Sellswords and other Experts of come here first.

An air of expectation and vague tension lays over Veloren. As if an impending storm awaits, too far away yet to make out any clouds. But the brush and trees are rustling in a growing wind.

Where and who will you be? What will you do and what life do you seek on Veloren? Only you know that and it is time you take your first steps.

## 3. Veloren - Cities, Towns, Villages 

## 3.0. Introduction

Cities, Towns and Villages are the core of every growing civilisation and culture. 

Here individuals come together to live, work and thrive more effectively. With the numbers of its inhabitants grows the exchange of ideas, scientific and cultural progress, the productivity of the craftsmen and the competition for higher positions in a society's ranks. 

This text outlines under what perspectives and goals Velorens cultures plan, build and organize their cities, towns and settlements. 

## 3.1 Gold Elves

### 3.1.1 Cities

The Pillar Cities are a central part in Gold Elven Culture. Most Gold Elves still live in the bowels of these gargantuan Structures. For over thousand years these cities are nearly worshipped like godly entities, a belief forged with tight rules and held up under the strict guard of the Dark Elves. Any action perceived by Dark Elves as an act against the Pillar cities unity, order and security is seen as a crime and transgression against the law. And no one, not simple worker, uprising trader or a member of the new nobility is safe from harsh prosecution by the Order of the Dark Elves.

The Pillars once were created with the help of long lost geological magic, its bright stone grown in one piece and right out of the ground, with stone roots that reach far beneath the soil and bedrock. The outer stone is near white, bleached from thousands of years of sunlight but with dark streaks where rain and other waters sought their way downwards. 

The top of the Pillar Cities was for thousands of years only accessible to the predecessors of the Brushwood Elves and the Dark Elves. The Brushwood Elves’ ancestors were expert farmers that worked constantly, diligently and as effectively as possible to grow the crops that fed the massive population under their feet.
The Dark Elves' predecessors protected the Entrance to the City and were the only Elves that ventured out to seek resources or oppose  any dangers that may arise in the surrounding, ashen remains of Veloren’s surface. 

With the opening of the Pillar Cities, over half a millenia ago, the fields were gradually removed and replanted in the surrounding, recovered countryside. The topside slowly became home for the newly emerging Nobility who built here their mansions, high rises and palaces. Next followed by high ranked officials and expert Logisticians, the latter of which gained success as rich merchant families. Buzzing, splendid Cities adorn the Crown of most Pillar Cities, with streets tens of meters wide and great plazas between the white, multistory stone buildings. 

The foot of the Ramp that winds upwards to the top is flanked by Offices and Warehouses where Officials and Dark Elf Overseers control anything and anyone that is on it’s way to the inside. Around that are wide and open training fields where the Gold Elven Army trains and drills thousands of their soldiers. Wide stone streets between every building and courtyard enable the constant movement of deliveries, carriages and manpower. Where the ramp meets the Pillar great cargo lifts tirelessly carry thousands of tons of material up and down from the city.

One of these streets circles the entire Pillar City and then rays out in all directions like the spokes of a wheel. Where not hindered these streets go outwards for several kilometers before ending in a second ring street. Between these spokes lay endless fields with crops of all kinds, headed up on the inside by large barns. An army of strawhats and rough sunglasses wearing workers tends these fields day in day out just to be ushered up the ramp and back inside every evening. Soldiers and Dark Elf Overseers patrol the outer ring street just as much to keep people inside as potential enemies outside.  

A narrow, straight gorge cuts through the middle of the Crowning City, without breaching the outer walls of the Pillar. The gorge is barely 15 meters wide at the top and increasingly narrow the further down it goes. Except for noon, the Sunlight does not penetrate much deeper than at most 20 levels and a steady breeze carries thick and used air and smoke out of the deepest levels up through the gorge.

Long ago cut directly into the raw stone, the inner bowels of the Pilar City are an endless net of narrow Hallways and rooms from the size of a storage room to halls hundreds of meters wide and long, used for communal rooms or workshops. The average ceiling is at the most 1 meter higher than the average Gold Elf. Many amenities, like sleeping niches or the rows of seating for communal dining halls have been cut right into the stone. This is where to this day most of the Gold Elven populace lives, works and dies.

Between every level narrow tunnels have been carved into the stone for air flow and sewage drainage. The air shafts open into the gorge and in narrow, hidden slits in the outer rock of the Pillar. The sewage canals end in bottomless shafts that can only be accessed through said sewage tunnels.

Only around a third of the inner levels of a Pillar City is in actual use. The lower of the inhabited levels are often dangerous, near lawless slums where the destitute and haunted live and the ostracised and hunted hide. Gangs and criminal organisations hold power here and clash with authorities and agents of the Dark Elves.

Below even that are the abandoned Floors and Hallways. Dark and dreadful places that still carry the signs of a crisis-ridden past of riots and hunger. An atmosphere of desperation, sickness and ash hangs in the air.

The only Elves traveling these paths are Dark Elves and Soldiers bringing down ressources and messages for the Pale Elves. In rare cases they escort Officials and Nobles downwards who seek the knowledge, advice or the blessing of the Pale Elves. Sometimes they bring prisoners.
Every one of these treks is dangerous, for there are creatures lurking in the darkness of whose origin no Elf wants to think too much about. Further danger comes from the abandoned levels’ condition. Paths and hallways downwards change constantly. Floors that have been put in can suddenly break away, either from material fatigue or from being undercut by water over a long time. There are rooms that were closed off by collapsing rubble and then ran full with rainwater and age old sewage. At some points the floor or the rubble gives way to the pressure, releasing a deadly and illcarying flood down the hallways. 

And there is another creeping horror, where open, once square cut rooms and hallways seem to round out or disappear entirely. Floors seem to weirdly swell up and strange outcroppings appear and reach like roots through formerly perfectly cut hallways. Having long forgotten how the cities were exactly built travelers regularly send worried messages about these occurrences to officials. Most of these are then ignored as hallucinations caused by the unease of the darkness and sewage fumes.

Cities taken over by Gold Elves from other factions are for the most part left untouched. As untouched as is possible when a city is conquered. Gold Elves understand the difficulty of a well run city and therefore try to keep local structures and organizations in one piece. However with Dark Elves comes a high military presence that restore and take over local fortifications or build their own to hold the conquered territory. Local personalities of power and status are often left in peace as long as they are useful to keep the city running. However, often shikane towards workers and craftsmen by Dark Elves and the Gold Elven Army often leads to those fleeing the city or uprisings against the Invaders. In recent history Gold Elfen traders have tried, with growing success, to stop such occurrences and to keep the local existing economy alive as is to direct its trade towards Gold Elfen cities. 

Gold Elves don’t really found big new cities. They prefer to consolidate the mass of their Industry and Power within and in the shadows of the Pillars. Cities outside of that have always been taken over in past struggles or are unintentionally grown from towns or great Gold Elfen military bases.  

### 3.1.2 Towns

There are three kinds of towns within Gold Elf territories.

Towns within conquered Territories that are not near any strategically or economically important areas are left to their own devices aside from a small military presence and a few officials like tax collectors.

Towns within conquered Territories that are of strategic and economic importance will be taken over by Gold Elfen officials and guarded by an appropriately sized military force although here too Gold Elven merchants increasingly try to protect local structures of trade and commerce. In such Areas the Gold Elven Army places great Fortresses to oversee a number of these towns at once. These then house the local Army and the offices of regional Dark Elf officials and become the contact point between the people living in the area and Gold Elven rule. 

The last kind are towns that have sprung up around a Gold Elf Fort or Fortress. Especially in the more remote corners of Veloren, where civilization hasn’t really arrived yet people tend to gravitate and settle around these Forts for security. And over the last few hundred years as Gold Elven territories spread out several of these settlements have grown to new proper towns in their own right. As these towns grow soon merchants move into them followed by officials that then must acknowledge and officiate this new town as such. Soon they will be followed by more merchants and a growing populace of Gold Even Workers. Even then most of the town will be made up from simple wooden housing for workers, warehouses and workshops of various kinds. With the workers more Dark Elves and military will follow to keep the workers in check.

### 3.1.3 Villages 

Gold Elves don’t really have naturally developed villages. In conquered territories Areas that house a number of villages are often overlooked by a small gold Elven Outpost which provides patrols for local stability, houses Gold ELven officials such as tax collectors and offers basic medical support for the local population through the presence of military medical personnel at the fort.

Just as with towns there might grow a village around such an Gold Elven fort. The increased security and stability the troops presence brings to an area leads to people settling within their reach.

## 3.2 Brushwood Elves

After breaking free from the Pillar Cities the Brushwood Elves journeyed far off to escape beyond Dark Elven reach, should these decide to pursue them. The refugees rested sparsely as they knew to be on a tight time limit before they would run out of clean water and food. So they travelled the ashen lands for weeks and even months surveying water and the ground and dirt beneath the grey dust, wherever they went.

Eventually they found soil and water that they deemed healthy enough to risk growing crops in and watered by it. So they set down a camp near a running water,  with simple tents for shelter. The few hunters they had set out to hunt whatever they could find in the surrounding wasteland and training more in the hunt, while the rest started to fortify the camp and work the land. 

The plan once had been to first plant few seeds of every crops at first to see if they would root in the ground. But this had been the best ground they had found on their journey so far. And the journey had already taken far too long to waste more time. The Elves started tiling many hectares of land to fields, digging kilometers of irrigation trenches fed by the close by water and planting much of the carried seeds. For many weeks tension laid over the camp, as they watered the seeds and onions, reinforced the trenches waiting if crops and fruits would grow. Finally the seeds, onions and tubers had taken to the soil, were sprouting and growing and the Elves could breath relieve.

It was then that the elders sequestered two circular spots of land with walls of rough fabric each on the opposite edge of the ground worked by the Elves. Finally a young elf stepped before them, in their hands an heirloom their family had handed down from generation to generation over thousands of years. The elders made space for the young Elf and they grabbed into and pulled out a round seed, the size of an adult fist. Then they planted one of these seeds in the middle of each spots. With that the first Elder Trees, central elements of the future Brushwood Elven culture were planted.

Decades later the Eldertrees had grown high enough to cover the settlement completely i shadow. The tents were long replaced by wooden houses made from the dead trees of Velorens lost forests, preserved under the ash and a scorching sun.
Fields of crops, fruits and small trees reached to every corner of the valley. Wild grass and bushes had grown between the fields and houses and had started to spread far beyond. A precise and shimmering network of stonewalled irrigation canals kept the Valley green, even in the driest Summers.

The Elves population had grown in numbers enough so that small groups ventured out to find new and nearby areas to spread out and start new settlements and bring more life back to Veloren. Was a group large enough they would be given two Seeds of Elder Trees to take with them if they could ensure they had enough workforce to take care of it.

No one had known how fast and how tall the trees would rise in to the sky. The very first had in fact already reached such a size that parts of the settlement would need to be moved to avoid houses being toppled or damaged by their spreading roots.

So every group of settlers got the advice to plant them far further apart from one another then the first two had been.  

### 3.2.1 Cities

Most cities of the Brushwood Elves are centered around an Elder Tree.

Of both trees that are planted with the founding of a new settlement one is to become this settlements center, the other will be grown and protected just the same, but marked to be harvested at a point far into the settlements future.
The tree that is to become a settlements center simply carries the name of said settlement. Is the City named Saatkern the tree will be named Saatkern Tree. If the tree has grown to a appropriate size a small house will be built at the trees side. The houses foundational platform will then be affixed to the trees trunk. This will from then on be the house and home of the trees ceremonial and practical caretaker.

As the tree grows, pulling the house upwards, it will be supported at first by increasingly longer stilts. Is the tree again grown enough so that the hut has enough room under it, the stilts and columns under the house will be replaced by a further set of rooms with their foundational platforms also affixed to the tree trunk. These will then house the assistants and apprentices of the ceremonial and practical caretaker.

As the tree and the settlement around it grow large so will the house eventually grow to a expansive wooden complex. Therefore every Brushwood Elven founded city has a finely crafted, wooden palace at the food of their Elder Tree that houses their Leadership, bureaucrazy and everyone one else high ranking that serves the city in official function.

Roughly a hundred years into a cities lifespan their Elder Trees crowns start to reach one another. After careful and thorough consideration through the leading caretaker the cities inhabitants will prepare and perform the harvest festival. With the conclusion of this honoration the Elven city will start to harvest the second tree piece by piece. 

This harvest can take decades and gives the city a huge economic boost as wood from Elder Trees of this age possess extraordinary, worldly and thaumaturgic qualities. This harvest only takes place under the strict leadership of expert Lumberjacks and Woodworkers. For one because accidents could damage the city under it and more importantly because every part of the Elder Trees is sought after far beyond Brushwood Elven lands by craftsmen of all resort, magical and otherwise. A simple branch damaged by sloppy workmanship could lose the community a lot of value. 

Ideally, by the time the harvest of the marked tree is being executed more and younger Eldertrees will have been grown in the surrounding lands.

The buildings of a classic Brushwood Elven City are primarily built of wood. Upgraded and larger buildings are placed on a foundation of cut rocks.

Flooring in living areas are made of polished wood and often laid out or laid in with felted Mats, depending on the wealth of the owner. Workshops are preferably directly placed on the stone of the foundation or on hardened and rough wooden flooring. Workshops that require great heat or large open fireplaces are situated outside of buildings and walled of with stone walls. In the city these are often in the backyard behind or next to the house of the workshops owner.

Multi Storey houses have a heating tower, a corner tower of stone that houses the furnaces, ovens and heaters of the building. Small houses simply dedicate an entire corner of the house to the kitchen and oven, with stone flooring and stonewalls.Small buildings are mostly one open room with exception of maybe the sleeping quarters. In two storey buildings the second floor is often open above the kitchen so that heat can rise easier and warm the entire house.

If there are inner walls in buildings they are of paper and carton in varying thickness, made from wood pulp. Especially, large multi storey houses like the palace around the Elder Tree use these flexible walls so that rooms can easily be changed in their layout. Where necessary paper walls will be used that are thick enough that they prevent easy listening in to conversations and guarantee privacy of normal volume conversations.

The Palace at the base of a cities Elder Tree is usually surrounded by districts with multi storey housing which regularly reach up to five floors. Here live most of the workers and officials that work at the palace but are not of a high enough rank to be housed in it. The bottom of these floors often feature  restaurants, teahouses and other businesses that serve these workers, visitors and the palace.

Beyond this are more loose and open districts of the workshops, smaller houses of the average brushwood elves and their families some with the small workshops and businesses a cities inhabitants need. Here and there between the smaller houses protrudes a merchants multi story home and warehouses from where they bring wares in and out of the city.

Depending of the age of the city, between these outer districts sits the City Fortress. A defensive structure, garrisoned by the Brushwood Elven Army, carved into the rock hard stump and tunneled into the roots of the marked and harvested Tree.

Brushwood Elven Cities defenses are several rings of wide canals. The outer defense of the Brushwood Elves though are not walls but the deep and endless forests that surround them and that most of its inhabitants know like the back of their hands including the places that should be avoided.

Foreign Cities that have been taken over by the brushwood Elves, for whatever reason, are often and depending on the severity of the reason for the take over  left in peace. Either way Brushwood Elves usually take over a building that houses the bureaucracy necessary to connect the city to the rest of the Brushwood Elven realms. They also will station an appropriate amount of soldiers to mount the local defense if necessary and depending on the importance of the city and its local.

Where appropriate they import and share their own knowledge for farming and their system of irrigation canals. With the connection to Brushwood Elven territory cities foreign cities often develop a flourishing culture of houseplants, plant pots and tubs. The closer a city is to Brushwood Elven territory the more its houses and streets tend to be adorned with greens and flowers.

Brushwood Elves try to integrate new areas and their inhabitants into their societal structures. However, Dark Elves are in most cases not tolerated as their fanaticism till now always have lead to further conflict.

### 3.2.2 Towns

Recent history has seen less Brushwood Elven settlements start out with the traditional planting of two Elder Trees. They still exist but are increasingly rare. One reason behind that is that not every landscape can support the growth of these giants to their full size. Another is that outside of their population centres the Brushwood Elves tended to growing and spreading more even balanced Flora and Fauna. To grow more diverse forests that can sustain themselves and grow on their own.

Nowadays towns can therefore be found mostly within large and sheer endless forests. These towns and their surrounding fields are often the only greater clearing in a sea of trees.

The Elves of these towns work the surrounding woods like fields, carefully harvesting, felling  trees ready for it and planting a wide variety of new species where appropriate including Elder Trees. Most towns also serve as trade hubs for the villages in any given area.

There is another type of town. Brushwood Elven towns that are built for a very specific reason or need. If an area is off strategic or economic importance but in an area that Brushwood Elves don’t necessarily like to be in, say outside of their vast forests, in arid landscapes or even deserts, leaders send out the army and other specialists to build so called Worktowns. These towns are strictly planned and laid out to fulfill their purpose and not much else. For example a mining town in a dry area, or a fortress with some Multi storey  buildings for the necessary workers. While some Elves might become used to living in these towns most live here for a few years at most, enticed by higher pay, and then return back to more loved areas. 

### 3.2.3 Villages

The great majority of the Elven people prefer to live outside of cities and towns, in the villages that are strewn all over the forests. Even nowadays most of the brushwood Elves share a certain aversion to cramped spaces of population centres.

Brushwood Elven Villages are more lose conglomerations of Brushwood Elven houses nestled between the trees and crevices of the forests. All kinds of Elves live this way, from secluded magicians and craftsmen to farmers and hunters. 

If the Elves of one of these conglomeration of houses feel like a community they might built a place to meet for festivities, council meetings on recent events or simply to trade wares. The one or other merchant might add a little tradepost to trade wares for the villagers in the next town or city. Some of these villages conglomerate outwards from crossings of trade routes and many more simply exist somewhere in the forest.

While Brushwood Elves on average and contrary to popular cliches do not live in trees, every village has at least a few guard posts and sanctuaries in case of danger or emergencies.

In the last decades with the subsiding of the conflicts more and more Refugees of Gold Elves set up villages on the edge of Brushwood Elven territory. These villages are often a mess and come under attack of Dark Elven lead raids trying to capture what they see as traitors on the Pillar Cities. To prevent diseases and escalation of political conflict Brushwood Elves try to find and break up these settlements to bring the refugees deeper into Brushwood Elven lands or into regions that do not border on Gold Elven territories.     

## 3.3 Dwarves WIP

### 3.3.1 Cities

### 3.3.2 Towns

### 3.3.3 Villages

## 3.4 Humans WIP

### 3.4.1 Cities

### 3.4.2 Towns

### 3.4.3 Villages

## 3.5 Orcs WIP

### 3.5.1 Cities

### 3.5.2 Towns

### 3.5.3 Villages

## 3.6 Undead WIP

### 3.6.1 Cities

### 3.6.2 Towns

### 3.6.3 Villages

## 3.7 Danari WIP

### 3.7.1 Cities

### 3.7.2 Towns

### 3.7.3 Villages

## 4. Veloren - Cultural Design Philosophy

## 4.1 Introduction

These texts are not meant to be taken as that these cultures ONLY use and own furniture of their own designs or that they can only make the culture specific designs.
Instead this is the reasoning why certain styles are coming from a specific culture, why they originally developed them this way and why specific furniture might or might not be found with certain people.

In the Asset Worklist I will use the following definitions of quality:

### 4.1.1 Improvised

Anything available was used or misused to create this item or get the functionality the person needed.

For example: an old, wooden crate used as a chair.

### 4.1.2 Functional 

It is exactly what it is supposed to be. It works, but there was no thought wasted on comfort or beauty here. Pure functionality. 

For example: a chair with a plain wooden seat and a back.    

### 4.1.3 Good 

A good, solid item. This was made not just with function in mind but also some sort of comfort was considered. Simple cosmetic elements like one or two small engravings or even just rounded corners were worked into the item. Comfort was definitely a consideration, within an appropriate scope of cost of course.

For example: a chair with armrests, indented seat and a formed wooden chair back for a bit more comfort. The top of the chair back is adorned with two simple, wooden, cosmetic spheres.

### 4.1.4 Expensive

This Item has been lavishly made, adorned with complex engravings, inlays of expensive materials, cushions etc. Interestingly enough while most of these items have been made with comfort AND artistic quality in mind some of them are in fact ONLY made to impress visitors and as a display of status. 

For example: A chair made of finest birch wood, with oak linings, carved with the finest details, a metal ornamental inlay glistening in the top of the back. Seat, armrests and most of the back have been made comfortable with stuffed cushioning made from expensive fabric of strong colors, fastened with shiny metal rivets.

### 4.1.5 Ceremonial

Ceremonial Items are often on a different level. Seldom is comfort or functionality a focus of these works, instead sending a message expressing the importance of a person of rank or social position. The goal is to represent the symbolic value and weight of the worshiped person, entity or position with breathtaking pomp. This does not mean that all ceremonial works are of gold and gems to show that value. It is rather shown through the enormous effort and thought that was put into these items.

For example: A ceremonial inauguration chair. Made from expensive metals, lined with gold, precious gems and ivory. The design is heavy, with stark corners and angles. The seating is uncomfortable and cold. But the meaning is heavy and massive, those who get seated here by a council of noblemen are to be crowned absolute ruler and Monarch of this Kingdom. Only drudged out on important days like this the chair has its own guard. The Throne does not just signify the next King but also symbolises, in a very practical way, a significant part of the Kingdom’s gold reserves, the value behind it’s currency.

### 4.1.6 Military

Military means it's extremely functional, uniform, hard to damage and/or massive in some way. This means a lot of the military things are just the functional versions of something, like functional chairs. But in other examples, like doors or a firebowl it means these are massive and/or really sturdy versions that are simple in use, meaning they have no complicated mechanisms in them that would easily break when transported or under duress.

## 4.2 Gold Elves

The Gold Elves have been basically incarcerated in their own cities for thousands of years.

For much of that time fresh resources like fresh wood were a seldom treat for craftsmen.
So ingenuity in recycling and great effectiveness when using ressources was one important aspect.

Nowadays thousands of tons of material and resources stream into the cities every day so the rarity is not such a huge factor any more. However, thousands of years of doing something in a specific way leaves its marks and creative effectiveness when using materials remains a constant in Gold Elven Craftsmanship.

For example: even nowadays even the most expensive chairs have only three legs and a single, relatively thin element for the seat back.

Small items like dishes and cutlery are still made from stone. As more resources and a greater number of finer tools become available, these are adorned with other materials and increasingly detailed engravings and inlays.

Most of these items are reserved for Gold Elven’s upper classes and trade, while those making them, the workers and citizens of the Cities inner halls, often still have to deal with the inherited, recycled and improvised items and the simple items left to them made from materials of lesser quality.  

## 4.3 Brushwood Elves WIP

## 4.4 Dwarves WIP

## 4.6 Humans WIP

## 4.7 Orcs WIP

## 4.8 Undead WIP

## 4.9 Danari WIP

## 5. Veloren - Farming Differences

## **5.1 Gold Elves**:

In the past Gold Elves used the tops of the Pillar Cities to grow their crops far above the then ashen lands of Veloren. Since they started to leave the pillar cities they import most of their food. Recently though they started to bring workers to the surface to work them as farmers.

They are still rather bad at it, so the fields they have are inefficient though they keep improving. It also does not help that they treat their farm hands like prisoners.

## **5.2 Brushwood Elves**:

Brushwood elves do have a bit of farming, but most of this farming consists of small gardens for vegetables, medicines and herbs. They grow and farm a wide variety of massive, fruit bearing trees and, opposite to common ideas of the Brushwood Elves, they hunt a lot of game. They are also not shy of importing food from other lands and are currently experimenting with animal husbandry under the crowns of their large Forests.

## **5.3 The Dwarves**: 

Dwarf culture functions a little like a massive united workshop. They have massive fields and farms further away from their towns and cities due to the smoke of those workshops. The dwarves are not ignorant to the negatives of their workshop culture.

During** The Long Dark, **subterranean mushroom farms were the main source for vegetables in the dwarven diet. Every dwarven city still has some of these, but that is more out of remembrance and tradition. Turns out most things taste better when they grow in sunlight.

## **5.4 The Humans**: 

Humans have loads of farmers that live in villages all across Veloren. They work the land and raise farm animals. Humanity’s most beloved vegetable is the amazing, beloved and versatile Potato. You can make humans VERY angry by calling Potatoes foul names.

## **5.5 The Orc Tribes**: 

The Orcs hold it with farming roughly like the elves, though they hunt much more. Orc hunting parties often coincide with their hunts for the creatures of darkness, which they sometimes also eat. Orcs can make a meal out of near anything.

The Orc tribes have fields at their homesteads, but they seldom plant anything that needs more intensive care.The old and the very young stay at home and tend these fields.

## 5.6 The Undead Protectorate:

The Undead have a very large number of all kinds of gardens. It’s a work that is beloved in their lands.There exists quite a sizable minority of the living in the Protectorate, that need to be fed. Since other cultures are not that open to the Undead Protectorate importing great masses of food is out of the question. Therefore an appropriate amount of farms with crops- and herded fields dot the landscape.

## **5.7 The Danari**:

The Danari are primarily Fishermen. Fish is the main staple of their Diet and Kelp and similar greens of the ocean are their Vegetables. They trade with almost everyone for foreign food and recipes and are generally open to culinary adventures.

## 6. Veloren - The Language

To see a Timeline of the events referred to the following document. Numbers in the text point to the events on the timeline with the same number.

All of the Races in Veloren speak the same language just with different dialects (**aside from convenience for game designers and players**).
The following text details why:

In the **(4.1) Age Of Wonder** Dwarves and Elves lived together in a peace that lasted thousands of years. Over such a long time both cultures were closely connected and their languages and scripture melded into one, just regional differences in some expressions. This is the Old Common Language.

At the end of **(4.2) The Great Malady, **the violent and decade long invasion of the nomadic Velvet Black Banner, the Orcs freed themselves and helped the dwarves defend their Mountain Fortresses against the rest of the (**4.4**) crumbling Armies of the Velvet Black Banner. Ashamed of their participation in the Invasion, the Orcs had no qualms to lay off the common language of the Velvet Black Banner and take on the Dwarven dialect. The orcs stayed with the Dwarves for half a century before leaving them in a search for redemption by ridding Veloren from the foreign creatures and terrors that The Velvet Black had brought with them. Even then the now nomadic Orc tribes had regular contact with the dwarves and the new language stuck.

Thousands of years later, **(4.7)** refugee groups of Gold Elven farmers flee the desperate conditions of the Pillar Cities, ending the age of **(4.6) The Long Dark**. They bring the Old Common Language in word and scripture with them. The strict dictate of the Gold Elven Leaders had kept this Language in its old form for all this time. However, free of the Tyrants the Brushwood Elves use more of the Worker Slang that they otherwise only used in close quarters with Gold Elven Workers they trusted. For the most part the same Language, the Brushwood dialect allows for more warmer, lively and vulgar expressions. 

The nomadic Humans, landing on the shore of an ashen and empty Veloren, meet the Brushwood Elves and immediately connect starting (**8.**) **The Age of Peace and Healing**. For hundreds of years Brushwood Elves and Humans live and work together. The Elves teach the humans many things one of which is the elven scripture of the Common Old Language. Over time the humans take over the language of the Elves and mix it with their own expressions and dialects and important names like Potato.

Worried about the desolate and degenerated state of the Orc Culture and its people a **(4.9)** Group of Orc Chiefs ask the Brushwood Elves for help. The Elven Queen takes pity and teaches the Orc Leaders the tools to rebuild their culture. Over many decades the Chiefs learn and study. This refreshes the use of the Old Common Language that they had learned from the Dwarves nearly a millennium ago. As they return to their people to reawaken Orc Culture they also rebuild the quality of their communication. Till today the Orcs speak this language. Their dialect is sharp, short and effective. It is the language of roaming Warriors that are always searching for remnants of dark dangers, of Hunters that always need to be ready and speak efficiently and precisely.

The Gold Elves leave their Pillar Cities and immediately attack the human settlements starting **(4.10)** **The War of Awakening**. They bring the same version of the Old Common language with them that had been spoken so many years ago in the **(4.1)** **Age of Wonder**.

This calls forth the Dwarves out of their own relative isolation in their mountain cities as they storm forth to aid the humans in their defence. When the Brushwood Elves also join in the support the Humans the war ends with a standstill and uneasy peace. This starts **(4.11) The Age of Competition**, the current and ongoing era of political, cultural and economic competition.

**(4.12)** The Danari make contact with the civilisations on the coasts of Veloren. The Danari Republic has their own language. However, in their motivation to share the benefits of democracy with the landlubbers of Veloren, and the Old Common Language being so present all over Veloren, every Danari child learns it as a second language in school.

**(4.13)** **The Undead Industrial Revolution** was an event that overwhelmingly hit the human kingdoms. So all of the Undead seeking Refuge in the Undead Protectorate were humans and therefore speak the same language and dialects.

# Veloren- Direction for the Writers

## 1.1 Introduction

The long term goal for Veloren is to construct a fantasy world where political and economical simulations cause many flowing events within the world that give it depth and life beyond the players view and presence.

A world that does not feel like a thinly veiled stage where the actors are only waiting on the players input to lift the curtain and start the play.
A world that offers more than just a main storyline a big baddy and beyond that stagnation.

In this document I will try and give a few directions and try to find starting points for #writing to go from in works and discussions. 

Important Points/Goals for the development to keep in mind are:

1. Veloren will feature the simulation of political and economic developments, including wars and/or political and diplomatic strife between factions.

2. The world is going to be constructed by RNG, including the placing and naming of individually named Characters, Cities, Dungeons etc.

3. The player should be able to interact with the world via a wide range of options, be it questing for Factions, Job Offers, Power Players within a faction (that at some point could be the player themselves), Trade, Exploration and many more.  

## 1.2 Basic Directions

Here I will explain very plainly a few directions that I wish to be followed and why these are important to me and the overall game.

### 1.2.1 The Great Malady

Most of all Individuals in Veloren have no idea about The Great Malady, though there exist very vague Legends here and there, these often are not even close to what actually happened.
Exceptions for that are the few hidden Pale Elves, some of the highest ranking Orc Shamans, Elders of the Brushwood Elves and the Danari and very few Loremaster of the Dwarves.

### 1.2.2 Recent History 

This means:
Most books, conversations and texts that are easily accessible to the players, even when they are about History, describe events going back at most around 600 years.
The further back this goes the more these are vague and full of holes.

Most that are concerned with history are more interested in the last 500 years of history, the history of the Age of Competition, during which the Kingdoms and who reigns over what etc. have been determined through political and cultural strife.

### 1.2.3 Ancient History

I want that text and Lore describing the Drama and Events of The Great Malady and the times around that, are reserved for the players to be found in old ruins, in preserved letters, inscriptions etc. More to this later in better detail.

Exceptions could be found in the rooms or in the possessions of a few rare people of power and wisdom but should not easily be given away or available to read for the player. Found material could be collected in a sort of collectible album, possibly accompanied by art where appropriate.


### 1.2.4 I can’t get no, Exposition!

If possible I want to avoid Lore text that reads like Exposition and History explained via Pie Charts and Sheets. Instead I’d encourage you to think about how a person would write and talk as the person that has been there and stuck in the thick of it. How it would feel when you see the civilization, that you have grown up in, crumbles and burns around you.

For the dialogue of the now, wording should stay relatively normal, not too far into fantasy nonsense but also not too modern, a middle thing of modern expressions and flowery conversations. Most of this will be up to the writers and how they perhaps want to write a character or quest. 


### 1.2.5 Names of the Lost

One of the more difficult things may be the fact that we can not name individual characters, places or cities. Not for ancient History nor for the Stories in the present. These are all given by the game upon world generation.

That is one of the reasons why I made the above point, 2.4, and why it will be better to focus on writing from that very individual perspective.

Maybe there will later be a way to call upon these names and insert them into quest text, but I have no idea if that is a possibility or not, not being a programmer myself.

## 1.3 Explaining Central Choices for the Lore

### 1.3.1 The Age of Competition

The point by which the player enters the world of Veloren is roughly 500 years into The Age of Competition. A tumultuous time that continuously redraws borders and relations.

Design reason for this: 

The reason therefore is that the game places factions and their areas of influence by simulating 500 years of history. Since we have no direct control over this, an age of cultural and political competition explains why areas of Faction and Subfactions could end up messed up or strongly intertwined.

### 1.3.2 The Age of Wonder and the Great Malady

The “great bad thing”, the massive war that destroyed the ancient Dwarven and Elven Culture happened roughly over 2000 years ago.
The war destroyed both of these intertwined Cultures completely, laid their cities and lands to ruin and broke their deep, cooperative bond.

Design reason for this: 

This is thought as the basis for many of the dungeons and ruins the player can explore, old Dwarven and Elven cities, destroyed and raided, marked by desperate, dramatic struggles from thousands of years ago, filled with old artifacts of both cultures and the decades of war.

### 1.3.3 Dwarves and Elves become Isolationists. 

Driven to near extinction both, Dwarves and Elves, pull back into their respective safe havens where they hole up for thousands of years while the land around them withers and dies from the aftermath of the war. Only occasionally small groups leave these places to forage from the areas around what they can’t get in their cities. Even after Nature, with help from early Human Culture and the Brushwood Elves, comes back to Veloren Dwarves and Elves leave their sanctuaries only slowly. The Dwarves keep the past a silent taboo, only to discuss with their own or very trusted outsiders. The Gold Elves even flat out denied to have had such a culture in the past, since their current doctrine swears upon the superior protection of their Pillar Cities.   

Design reason for this: 

The trauma of the war and the death and Corruption of Velorens Flora and Fauna keeps these two cultures isolated to few and rare points of civilisation.
This keeps the dramatic ruins and other marks of the Great Malady untouched and frozen in time for the players to discover and the newly expanding Fractions to desire.
The cultural erosion of Dwarven and Elven also ensures that they are technologically on an even level with the rest of the factions that have risen to the status of Civilisation. Finally it keeps the long gone past a mystery to uncover, even for Elven and Dwarven players.

### 1.3.4 The Velvet Black Army Falls

The Master of the Velvet Black Army is killed. This loosens the magical chain that kept the vast variety of races and creatures working together as a nomadic one.
Most of the Velvet Black Members and Creatures with thaumaturgic ability die, many lose their minds to a creeping corruption and few flee heavily wounded and with deep scars.
As the organisation of the Velvet Black falls apart the army turns on itself in skirmishes and conflicts that tear over Velorens surface for many, many decades.

Design reason for this:
The undefined variety of races and creatures brought in by the Velvet Black leaves space for other writers to input ideas for races, creatures and monsters brought to Veloren and then lost and/or corrupted during the fall. These survivors and creatures then roam the hidden corners, ruins and dungeons of Veloren, posing a danger to nature, spirits, explorers and the new, expanding Civilisations. A good basis for quests and stories based on these and hidden Lore on the drama of the Fall since most Creatures and Cultures pressed into service of The Velvet Black weren’t simply evil in nature. 
 
For modders it also opens a good in world reason to add custom playable races into the mix. 

## 1.4 Vision for the Mood of the World

Over the last 500 years the blooming civilisations were mostly busy with fighting over what they had and establishing themselves as worthy Competitors. There are conflicts here and there but overall, at the point that the players enter the world, Civilisation has stabilised. Nothing could come and easily erase the diverse range of Cultures, Ideologies and Factions.

Mapping the world is important. And there is much to do as magicians and researchers seek knowledge, Countries seek power and resources and Traders seek ways around border fees. A safe passage through the wilderness lets an army flank their opponents, traders evade tariffs and historians and treasure seekers alike have great interest in sunken ruins and forgotten Cities.
Civilisation yearns for precious materials like ores, workable land, new exotic products and places of power interest many a magician. 

### 1.4.1 The feeling of the civilised world.

For the civilised world and its writing I want us to aim for a friendly, warm but down to earth feeling, for the average and normal situations.

The situation for the people is overall on the up. The Situation for the last few years has been calm and mostly peaceful and from farmer to Monarch everyone seems to be light of mind. The lack of war and conflict put a lot of fame and thrillseekers out of work. So these adventurers turn to the unexplored reaches of the Veloren continent.

The smell of Adventure and Spring lies in the air, trade seems to be booming and benefits most people from day workers to the richest merchants.
Even remote Villages are relatively safe and farmers find good reason every few weeks for a village festival and some self branded alcohol.
Many a nobleman, rich investor, magician or secret contributor seek brave (or foolish) souls to explore the unknown Wilderness.

### 1.4.2 The feeling of Frontier and Wilderness

Behind Civilizations most remote Villages and Small towns lies the frontier, untouched, rugged and wild. Hunters, Foragers and those seeking peaceful lives far from rulers and taxes, from the noise and trouble of civilisation roam here. Faune exists plenty. Here and there settlers have built small, remote settlements, ringing with the elements.

It’s here where the Spirits of all kinds meets the People of Civilisation. Most meetings are respectful and with distance. Stronger Spirits are sometimes worshipped by Villagers close by. Humans have unusually regularly close and friendly contact with Spirits and Brushwood Elves often are like next door neighbours with all sorts of creatures.

However, the region is not harmless. Some spirits can be mischievous, few even seek to harm Spirit and People alike and in some rare cases Corrupted spirits are a danger for all of life's individuals. Predators like Wolves and Bears roam the Wilderness and no Guard patrol the areas to keep Predators away, may they be Sapient, spirit or animal.

Life here is free but also risky, wild and unpredictable.

### 1.4.3 The feeling of the Lost World

Far away from the civilised world in the Wilderness, lay the untouched ruins from an age of ruin and war.

In disparate Spots, hidden in the wilderness, entire cities and fortresses have been conquered, destroyed and been left to crumble long ago.

Most of these spots are of a sombre sadness, an indescribable feeling of humbleness and loss overcoming visitors as they see the magnitude of this fallen Culture and questioning what could have made these people fall.

Anyone who dives deeper into exploring these places can probably get a faint idea. Many of these places harbor secrets and danger, dramatic history watched over or defiled by dangerous creatures, by madmen, tortured ghosts, and corrupted tribes of unknown race and heritage.

And not few adventurers may find a cruelling fate at the end of this multitude of awakening dangers.

## 1.5 Direction for Creativity

I hope that this guide can help us to create a consistent feeling, quality and atmosphere for the world. 

Much more important is that I hope this serves as a inspiration and directing guide to ideas for the #writing team.
That it, through its limits, tickles out the creativity of the writers instead just restricting what can be done.

## 2. Veloren - Format and Structure for Quests, Loretext and Flavor 

## 2.1 Quests

Now, if we want to write the quests and text for something as massive as an Open World RPG we need to somehow make sure we all understand what we are looking at when we work together on quests and stories, when we communicate and exchange ideas and concepts.

This is what the following segment of the Lore bible is there for. To give our team a structure so we can work together easier.

This does not mean that you can only work in this format. I am not telling you how you work creatively. However, when discussing quests together and most of all when sending them in for approval etc. I want that these formats help us to shoulder the task of giving an entire world soul, stories and legends.

To make this clear: with structure I do not mean the exact way a story within a quest is told, even when we aim for tonal consistency, but how we document the build and layout of a quest.

And lastly, having the quests in the formata proposed as following will undoubtedly make it easier to grab the text and implement it into the program itself however exactly this works. 

It is somewhat abstract to explain how I want Quests to be structured. So the explanations at the start might read confusing on their own. Because of that I wrote an example quest that should show off any of the elements in a way that makes sense. More ways to express what an author wants a quest to do as well as more  examples might be added later as we get more ideas and as the game gains the tools so we can actually put these into the game.  

So please be patient and read this chapter in full to understand where I am going with this.

## 2.2 Quests and Quest Phases

A quest is usually subdivided in phases. The simplest of that you might know as picking up a fetch quest that asks you to kill five of Whatever and collect their left, big toenails, the player kills the thirty Whatevers until they have the toenails and then delivers the quest to get their rewards. This is essentially a Quest consisting of a Questintro and a Questending.

To know how an author wants their Quest to be built we will use the following:

### 2.2.1 Questname

Obviously this simply gives the Quest a name. Depending on how many quests we accrue over time we might also need to add a number to the quests since I am not sure if a mere name would be helpful for the program to work with them.

### 2.2.3 Questphase

The title of this section explains where in the Questline the particular phase belongs to.

The first phase is always the QUEST INTRO.

The last phase is always the QUESTENDING.

Quest Phases between these are to be sequentially numbered starting with Quest Phase 1.

When there is a Quest Decision that could diverge a player on to a phase adjacent to the another one then these need to be numbered accordingly 1.1 etc.
For example: if there is a decision in the Questintro that could lead to two different Quest Phases the adjacent Phases should be named 1 and 1.1. If there is more than one path then the naming goes on accordingly 1.2, 1.3 etc.

### 2.2.4 Quest Decisions

If a phase ends with a player making a decision then these must of course somehow documented. That means this part needs to include:

The numbering and text of this decision.

For Example: 

Decision 1: “I just pay the money.”
Decision 2: “I will acquire the parts myself.”

And also every of these needs to have its own Quest Phase Result and Quest Log Entry (both of which will be explained next), INCLUDING to which questphase this decision will lead the player next. 

### 2.2.5 Questphase Result

As the name implies this entry needs to list:

- Quest Requirements so the player knows what is asked of them so they can reach the next Quest Phase or the finish Quest as a whole.

- When the player receives a Quest Item of sorts.

- When the Quest progression takes something from the player (for example a fine for lollygagging or something).

- Depending on what mechanics we get access to later, if the player has things happen to them like they get imprisoned or lead before a leader or something.

- And Ultimately what and if rewards in items, reputation, money, EXP and maybe access to gameplay features a player becomes for solving a quest.

If a Quest Phase ends in two Quest Decisions a Questphase Result must be written for each of these Decisions and most importantly it MUST note to which Quest Phase this decision leads the player next. 

For Example: 

Decision 1: “I just pay the money.”

-Leads to Questname Whatever, Questphase 1.3

### 2.2.6 Quest Log

We do not know yet how the Quest Log exactly will work.
However, ideally it lists the story content of all Quest Phases of any Quest as well as an active tasklist underneath that changes as the Quest progresses and new requirements are asked or new directions are given to the player.

Therefore this last section must list if the tasklist changes and if it does what changes, what tasks will be checked off and what new additions should be made to it.

## 2.3 The Example Quest -
Questname: An honest Path 

Now this is all very stuffy. Because of this I have written up and then formatted a Quest that covers these elements and simply shows what I have in mind with this and how it helps our work and hopefully prevents total chaos the moment we can actually implement Quests.

For a better understanding I made a layout overview of the Quest Phases.

<p id="gdcalert1" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image1.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert2">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image1.png "image_tooltip")

### 2.3.1 Quest Phase: 1a [Quest Intro]

As you leave your normal life behind, drawn forth by the calls of wanderlust and fame, by mysteries of velorens past and the promise of treasures and yet unknown sights and places, you briefly consider the option of a comfortable life.

Maybe you should go for a normal job? Something down to earth, something that earns you coin without needing to dodge the teeth of feral creatures in dank and dusty dungeons.

But even when adventure calls for you most, it could be useful to know the one or other craft besides knowing how to lop the head off a beastly opponent.

Someone needs to know how to stitch up fellow adventurers more than a temporary bandage could. Someone should know how to get the most out of the body of a bestial prize or how to mend a broken spear.

You mull all this through in your head. It can’t hurt to ask around what jobs are on offer and who is seeking apprentices. It can’t hurt to have options.

#### Quest Phase Result

- Add Dialogue Option for the player to applicable NPCs: 

Dialogue Option 1a1: “Hello Master. Do you need an apprentice?”
Leads to Quest Phase: 2a 

Dialogue Option 1a2: “Hello. Do you know if anyone here is seeking an apprentice?”

Leads to Quest Phase: 2b or Quest Phase: 2c

#### Quest Log

- Ask around if any Workshop, School or Business is seeking apprentices. Choose an occupation or craft that you want to learn.

---

The player decides that they want to be a blacksmith and heads to the next Smithy. They approach the Master that is working at the anvil and speaks to him.

Shop Dialogue:  “Welcome in my workshop. What can I do for you? Need a tool made or a piece of armor repaired? Or do you just want to take a look at my products?

---

### 2.3.2 Quest Phase: 2a

Dialogue Option 1a1: “Hello Master. Do you need an apprentice?”

The Master Smith looks at you then proceeds to hammer refuse of a raw piece of iron while hammering it into a bar shape. Then the smith drops the hot bar into the cooling basin.

He lays down hammer and tongs and eyes you up you from head to toe.

Stepping away from the anvil the Smith wipes his sooty and sweaty hands off on a rag at his side.”Well, you seem robust enough for the work in heat and anvil. However, before I can agree to anything I will have to assess your abilities.

Since I am not willing to lose coin on this you will need to provide your own tools and materials. You can also pay [a sum] to me and I will provide you with what is needed for the assessment.

#### Quest Decision

Quest Decision 2a1): “I will just pay the money for the tools and materials.” 

[If the player does not have the necessary money this option is greyed out.]

Leads to Quest Phase: 3a

Quest Decision 2a2): “I will acquire the materials and tools for the assessment myself.”

Leads to Quest Phase: 3b

Quest Decision 2a3): “I still need to decide what I want to learn, I am just trying to see what possibilities are open to me.” 

#### Questphase Result

- Quest Decision 2a3) lets the player leave the dialogue without having to choose 2a1 or 2a2.

- In case of the other Decisions see next Quest Phase according to the Quest Decision.

#### Quest Log

No changes either way.

---

The player seeks the next settlement, in this example a town. 
The player talks to a guard and uses the new dialogue option, asking if the guard knows anyone that is seeking an apprentice.

---

### 2.3.3 Quest Phase: 2b

Dialogue Option 1a2: “Hello. Do you know if anyone here is seeking an apprentice?”

Blacksmith: “Well, this town has plenty of workshops and masters of any craft. I do not know if any of them are looking for apprentices or students. You will have to ask them that yourself. I still can show you where you can find them though.”

#### Questphase Result

- Workshops and businesses within a certain radius/of the town are being marked on the player’s map.

#### Quest Log

- Ask around if any Workshop, School or Business is seeking apprentices. Choose an occupation or craft that you want to learn.

Add:
- Ask in Workshops and businesses for a tenure as an apprentice.

---

The player asks a random person outside of any settlements if they know of anyone that is seeking an apprentice.

---

### 2.3.4 Quest Phase: 2c

Dialogue Option 1a2: “Hello. Do you know if anyone here is seeking an apprentice?”

NPC Dialogue 2c1: “This is the middle of nowhere, stranger. There are not many that could need an apprentice out here. I can show you where you can find the nearest workshops and such. There you probably will have more luck looking for work.”

#### Quest Phase Result

The game marks the closest town or city on the players map. 

#### Quest Log

- Ask around if any Workshop, School or Business is seeking apprentices. Choose an occupation or craft that you want to learn.

Add:

- Ask in the next town or city if someone there is looking for an apprentice.

---

The player takes Decision 1a), deciding to simply pay the fee

---

### 2.3.5 Quest Phase: 3a

Dialogue Option Decision 1a): “I just pay the money.”

“All right! A confident decision. Here are your tools and the materials. Speak to me when you are ready for the assessment.”

#### Quest Result

- Add Dialogue Option:

Dialogue Option 3a1: “Master! I am ready to take the assessment.”

Leads to Quest Phase: 4a 

- The player loses the appropriate sum of coin.

- The player gains the necessary tools and materials for the assessment.

#### Quest Log

- Ask around if any Workshop, School or Business is seeking apprentices. Choose an occupation or craft that you want to learn.

Add:

- Talk to the blacksmith [name here] in [place here] to start your assessment.

### 2.3.6 Quest Phase: 3b

Dialogue Option Decision 1b): “I will acquire the materials and tools for the assessment myself.”

“All right. An independent spirit. Wait a moment.” The blacksmith pulls a piece of rough and frayed parchment from a shelf. Then he grabs a thin piece of coal from the pile next to the oven, writes down a list of items and gives you the paper. “These are the tools and materials you will need for the test. Obviously you will also use these tools for work should I find you able to become a blacksmith. Now off with you! Let me know when you have everything or in case you change your mind and just want to pay me to get you the stuff.”

#### Quest Result

- Add a Fluff item, “Frayed list, written in coal” to the player’s Lore items.

Add Dialogue Option:

Dialogue Option 3b1:  “I have changed my mind. I want to pay you the money for the materials and tools.”
Leads to Quest Phase: 3a

Dialogue Option 3b2: “Master! I am ready to take the assessment.”
Leads to Quest Phase: 4a

#### Quest Log 

- Ask around if any Workshop, School or Business is seeking apprentices. Choose an occupation or craft that you want to learn.

Add:

- Acquire these items for the job assessment:
[List of Items] 

- Talk to the blacksmith [name here] in [place here] when you have the items for the test or want to pay [A sum of coin] and acquire what you need for the assessment, directly from the smith. 

---

The Player takes the assessment to become a blacksmith's apprentice.

---

### 2.3.7 Quest Phase: 4a [Quest Ending]

Dialogue Option 3a1 or 3b2: “Master! I am ready to take the assessment.”

Player: “I am ready. I want to become a blacksmith. Test me!”

Blacksmith: “Very well then. I will show you how to craft [Item that can be crafted on low Blacksmith Level] and we’ll see how well you do.”

[Tutorial that explains the mechanics of crafting for the Blacksmith Skill Tree.]

---

The player follows the tutorial and crafts the first item.

---

Blacksmith: “Very Good. You do have potential, I give you that. I think you will do well working as a blacksmith. Though right now I'd trust you only with simple stuff.
For now you can take on the simpler commissions that we get in.


Work well and consistently and I will teach you more difficult and complex pieces which of course will earn you more money. \
Good work. I am looking forward to working with you!”

The smith gives you a mighty pat on the back that nearly throws you off your feet. But you manage to keep your balance.

“Now off with you. Here take this, go and celebrate your successful application on my coin! I’ll have to prepare the smithy for one more worker.“

With that he gives you a few coins and sends you on your way. As you leave the workshop you hear him mumble to himself. “A pleasant surprise…” then you are out of earshot.

Congratulations! You have successfully taken on an honest working occupation.     

#### Quest Result

- The materials are used up. 

- The crafted item is added to the players inventory.

- A list of commission work opens up for the player  the blacksmith workshops frontdesk.

- The blacksmithing tree opens up for the player.

- Entry level blacksmith recipes are available to the player.

#### Quest Log

- Congratulations! You started your first occupation and gained your first workplace. Good Work!

## 2.4 Types of Quests

For the moment we do not have information and work around the technical possibilities and specifics of quests. Therefore this section is still under construction.

## 2.5 Loretext

First i want to define what exactly i mean by Loretext. Loretext are things like books, notes, diaries, scrolls and similar items that allow the player a look into Velorens world.

Sometimes it's just something like a letter depicting the conversation between two people, a note with a farmers shopping list, the dramatic last few pages of a soldiers diary or the scientific thaumaturgic explanation of a magical art by a famous Mage.

The topic is pretty open and varied and so should be the possibilities for Authors to express an idea even when controlling for quality.

Furthermore will we make distinctions between Loretext based on 4. The Historic Timeline.

Authors will easier find inspiration for ideas and can communicate better what time they want to create Loretext about.

I still will have to determine what length an average page of Loretext is limited to. As these technical features for this are not in the game yet I will work out an average for that and then update it later when we know the limitations or options we have.

Ultimately I want for there to be a player diary or maybe Adventurers Ledger where all collectibles that a player has found are listed and can be read and looked at later.

For organisation Loretext is separated into types and then numbered.

These are the types of Loretext: 

2.5.1 Books

2.5.2 Pages

2.5.3 Lore Items

2.5.4 Quest Items

2.5.5 Flavor Text Items

2.5.6 Flavor Text Menus

### 2.5.1 Books

Books can be found in Veloren and can be the longest form of Loretext within the game outside of maybe quests. That does not mean every book has to be filled to the brim with text walls. If an author's idea requires their work to be in the context of a book but only fills pages sparingly (for example a book that has damaged pages) then that is also okay. Obviously books within the game do not have the length of real life books.

Documentation for books should be as follows:

Book 27.1 [ca. 300 AC] Elegant Vesture for the Noble Lord - Author Name (The actual, real life Authors name.)

Book 27.2 [ca. 300 AC] Elegant Dresses for the Noble Lady - Author Name 

Book 27.3 [ca. 300 AC] Proper Attire for the Servant. - Author Name 

The numbering will be added by those overlooking and managing the Loretext.

### 2.5.2 Pages

Pages are conceptually individual pages of paper or other similar forms of items that can carry text and that can be found within Veloren. Text for these should on average not be longer than two book pages. One for the front and one for the back of a page. That does not mean a page has to be filled to the brim with text. The text should note which part belongs on the Front and which belongs on the Backside of the page respectively. Pages can be practically everything that people jot down on paper. This could be the lost last pages of a book, a series of letters depicting a mail exchange, a shopping list or a note from one person to their partner before leaving for work.

Documentation for Pages should be as follows:

Page 8.1 [401 AC] Dear Ansarise - Author Name (The actual, real life Authors name.)

Page 8.2 [401 AC] Beloved Herrietta - Author Name

Page 10 [3 BAC] Battalion Pillars Shadow Inventory List - Author Name

Single pages don’t necessarily need to be named fancily. If it does not have a name it should feature a rough short description.

### 2.5.3 Lore Items

Lore Items are Artifacts made as a different way to show and tell Velorens Lore. An artifact can come with a description of the Item, description of inscriptions or text on the item or both.The description should be written in a-matter-of-fact style, leaving the emotional interpretation and evaluation to the player.

Documentation of Lore Items should be as follows:

Lore Item 7. [400 AC] Golden Shield Batch 

Lore Item 52. [The Long Dark] 

Lore Items don’t need to have a fancy name but should at least have a short, rough description so it can be found easier in lists.

### 2.5.4 Quest Items

Quest Items, as the name suggests, are specifically made for Quests. Their documentation should come with the Quest that they belong to.

Quests can also require items from Velorens normal item pool. These Items don’t have an extra description and come with their normal flavor text.

Documentation for Quest Items should be as follows:

Questname: An honest Path - Quest Item: Apprentice License for Blacksmiths

### 2.5.5 Flavor Text Items

Flavor Text for Items are short text blurbs that describe any item that can be picked up by a player. The description style can be varied and its style is free top the author as long as it is within the overall style of the Lore and Game.

Since Veloren will probably have hundreds if not thousands of Items we will (well, probably me, ugh) need to make a list of the appropriate Items, most assuredly in cooperation with the Visual Asset Team, and then work it off Item by Item.

How exactly we organise this will need to be evaluated in the future.

### 2.5.6 Flavor Text Menus

Flavor Text for Menus is for basically every time the player does something that activate a Menu or UI of sorts. So when the player speaks to a Shopkeeper and the Shopkeeper greets the player before offering a Buy or Sell menu that is Flavor Text for Menus.   

I want to remind everyone that the game aims to have a reputation system and that different people based on prejudices might react differently to player characters based on their attributes. That means Veloren will probably have hundreds of possibilities for different menu text and we (well, probably me, ugh) need to make a list of the needed spots that need to be filled with text, most assuredly in cooperation with whatever team will be responsible for that, and then work it off spot by spot.
How exactly we organise this will need to be evaluated in the future.

## 2.6 Loretext Examples WIP

### 2.6.1 Books WIP

### 2.6.2 Pages WIP

### 2.6.3 Lore Items WIP

### 2.6.4 Quest Items WIP

### 2.6.5 Item Flavor Text WIP

### 2.6.6 Menu Flavor Text WIP

## Written by Felixader

## Proofread by WelshPixie
